/**
  * Created by Databiz on 05/03/17.
  */
object HighOrderFunction extends App {
//  val fl = List(List(1), List(2), List(3)).foldLeft(Nil)((list, el) => el :: list)

  val f = List("1", "2", "3").fold("")( (elStr, outStr) => elStr + outStr)
  println(f)

  val fl = List("1", "2", "3").foldLeft("")( (outStr, elStr) => outStr + elStr)
  println(fl)

  val fr = List("1", "2", "3").foldRight("")( (elStr, outStr) => elStr + outStr)
  println(fr)

  List(1,2,3,4).init

  List(1,2,3,4).isEmpty
  List(1,2,3,4).nonEmpty

}
