
// TODO: deal with Either
class DNA(dna: String) {

  val validNucleotides = Set('A', 'C', 'G', 'T')

  private def validateDna(dna: String): Either[String, Int] = {
    ???
  }

  private def validateAndCount(dna: String, nucleotide: Char): Either[String, Int] = {
    dna match {
      case d if d.forall(c => validNucleotides.contains(c)) =>  Right(dna.count(_ == nucleotide))
      case d => {
        val firstInvalid = d.find(c => !validNucleotides.contains(c))
        Left(s"invalid nucleotide '${firstInvalid.getOrElse('_')}'")
      }
    }
  }

//  def isValidNucleotide(n: Char): Either[String, Char] = {
//    if (validNucleotide.contains(n))
//      Right(n)
//    else
//      Left(s"invalid nucleotide '$n'")
//  }

  def count(char: Char): Either[String, Int] = char match {
    case 'A' | 'C' | 'G' | 'T' => validateAndCount(dna, char)
    case _ => Left(s"invalid nucleotide '$char'")
  }

//  type CountByNucleotide = Map[Char, Int]


//  val defaultMap: Either[String, Map [Char, Int]] = {
//    Right(validNucleotide.map(_ -> 0).toMap)
//  }

  private def validNucleotide(nucleotide: Char): Boolean = validNucleotides.contains(nucleotide)

  def nucleotideCounts: Either[String, Map[Char, Int]] = {

    dna.toCharArray.find(c => !validNucleotides(c)) match {
        case Some(char) =>
          Left(s"invalid nucleotide '$char'")
        case _ =>
          Right(validNucleotides.map(nucleotide => (nucleotide -> dna.count(_ == nucleotide))).toMap)
      }
  }

//  private def validNucleotideStrand =


    //    val countByNucleotide = for {
//      n <- validNucleotide
//    } yield count(n).right
//    countByNucleotide.map(_.e)

//    dna.foldLeft(defaultMap) { case (n, c) =>
//      for {
//        nucleotide <- isValidNucleotide(c).right
//        counts <- n.right
//      } yield counts.updated(nucleotide, counts.getOrElse(nucleotide, 0) + 1)
//    }

//    val countByNucleotide = for {
//      n <- validNucleotide
//      cnt = count(n).right
//    } yield (n, cnt)

//    countByNucleotide.toMap
//    } yield (n, cnt.right)


//    countByNucleotide.toMap.map(el => Iterator(el))

//    countByNucleotide.toMap.map(el => el._1 -> el._2.right.)

//    val aaa = countByNucleotide.toMap.exists(el => el._2.iLeft) match {
//      true => Left("aaa")
//      _ => Right(countByNucleotide.toMap.map(el => el._1 -> el._2.right))
//    }

//    countByNucleotide.fold(identity, _)

    // f: Map[Char, Either[String, Int]] => Either[
//    countByNucleotide.toMap.flatMap()
//    val d = countByNucleotide.map{ el =>
//      val dunno = el match {
//        case (c, Left(x)) => Left(x)
//        case (c, Right(y)) => Right(c, y)
//      }
//      dunno
//    }

//    val dunno = countByNucleotide.toMap match {
//      case (c, Left(x)) => Left(x)
//      case (c, Right(y)) => Right((c, y))
//    }

//    val countByNucleotide = validNucleotide.map(nucleotide => (nucleotide, count(nucleotide)))
//    countByNucleotide.flat


//  }
}
