import scala.annotation.tailrec

object RunLengthEncoding {

  //    def encode(str: String): String = encodeRecoursive(str)
  def encode(str: String): String = encodeWithApi(str)

  def makeEncoding(lastChar: Option[Char], out: String, counter: Int) = {
    out + (if (counter > 1) counter.toString else "") + lastChar.getOrElse("")
  }

  case object EncodingContainer {
    val empty = EncodingContainer("", 1, None)
  }
  case class EncodingContainer(out: String, counter: Int, lastChar: Option[Char])


  def encodeWithApi(str: String): String = {
    str.span()

    val endingEC = str.toList.foldLeft(EncodingContainer.empty) {
      (ec, char) => char match {
        case x if Option(x) == ec.lastChar => EncodingContainer(ec.out, ec.counter + 1, ec.lastChar)
        case x => EncodingContainer(makeEncoding(ec.lastChar, ec.out, ec.counter), 1, Option(x))
      }
    }
    makeEncoding(endingEC.lastChar, endingEC.out, endingEC.counter)
  }

  def encodeRecoursive(str: String): String = {

    @tailrec
    def encode(list: List[Char], lastChar: Option[Char], out: String, counter: Int): String = list match {
      case Nil => makeEncoding(lastChar, out, counter)
      case x :: xs if Option(x) == lastChar => encode(xs, lastChar, out, counter + 1)
      case x :: xs => encode(xs, Option(x), makeEncoding(lastChar, out, counter), 1)
    }

    encode(str.toList, None, "", 1)
  }

  //  def decode(str: String): String = decodeRecoursive(str)
  def decode(str: String): String = decodeWithApi(str)

  def makeDecoding(char: Option[Char], out: String, digit: Option[String]) = (digit, char) match {
    case (None, Some(x)) => out + x.toString
    case (Some(d), Some(x)) => out + (x.toString * d.toInt)
    case (_, _) => out
  }

  case object DecodingContainer {
    val empty = DecodingContainer("", None)
  }
  case class DecodingContainer(out: String, digit: Option[String])

  def decodeWithApi(str: String): String = {
    val endingDC = str.toList.foldLeft(DecodingContainer.empty) {
      (dc, char) => char match {
        case x if x.isDigit => DecodingContainer(dc.out, Option(dc.digit.getOrElse("") + x))
        case x if !x.isDigit => DecodingContainer(makeDecoding(Option(x), dc.out, dc.digit), None)
        case x => DecodingContainer(dc.out, dc.digit)
      }
    }
    endingDC.out
  }

  def decodeRecoursive(str: String): String = {

    @tailrec
    def decode(list: List[Char], out: String, digit: Option[String]): String = list match {
      case Nil => makeDecoding(None, out, digit)
      case x :: xs if x.isDigit => decode(xs, out, Option(digit.getOrElse("") + x))
      case x :: xs if !x.isDigit => decode(xs, makeDecoding(Option(x), out, digit), None)
      case x :: xs => decode(xs, out, digit)
    }

    decode(str.toList, "", None)
  }
}
