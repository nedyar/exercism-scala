/**
  * Created by Stefano on 26/02/17.
  */

class Bob {
  def hey(str: String): String = str match {
    case x if allUpperCase(x) => "Whoa, chill out!"
    case x if x.endsWith("?") => "Sure."
    case x if x.trim.isEmpty => "Fine. Be that way!"
    case _ => "Whatever."
  }

  def allUpperCase(str: String): Boolean = !str.filter(_.isLetter).exists(_.isLower) && str.exists(_.isLetter)
}
