import com.typesafe.scalalogging.LazyLogging

class Anagram(val word: String) extends LazyLogging {

  def matches(possibleAnagrams: Seq[String]): Seq[String] = {
    val sortedWord = word.toLowerCase.sorted
    logger.debug(s"Sorting Word: $word -> $sortedWord")
    val sortedAnagrams = possibleAnagrams.map(el => (el, el.toLowerCase.sorted))
    logger.debug(s"Sorting Anagrams: $possibleAnagrams -> $sortedAnagrams")
    sortedAnagrams
      .filter(el => el._2 == sortedWord && el._1.toLowerCase != word.toLowerCase)
      .map(_._1)
  }
}