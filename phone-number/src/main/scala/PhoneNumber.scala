
class PhoneNumber(val numberStr: String) {

  private val digitChars = numberStr.filter(_.isDigit)

  def number: Option[String] = digitChars match {
    case num if num.length == 10 => Some(digitChars)
    case num if num.length == 11 && num.startsWith("1") => Some(digitChars.tail)
    case _ => None
  }

  def areaCode(): Option[String] = {
    number.map(_.take(3))
  }

  def prettyPrint(): Option[String] = {
    number.map { num =>
      val area = num.take(3)
      val first = num.drop(3).take(3)
      val last = num.takeRight(4)
      s"($area) $first-$last"
    }
  }
}