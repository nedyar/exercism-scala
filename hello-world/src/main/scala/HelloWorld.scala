/**
  * Created by Databiz on 26/02/17.
  */
object HelloWorld {

  def hello(name: String = "World"): String = s"Hello, $name!"

}
