scalaVersion := "2.12.1"

val wrongDirs = Set("project","target","default")

def isModuleDir(file : File) : Boolean = {
        file.isDirectory && !file.isHidden && !wrongDirs(file.getName)
}

def subDirectories(path : String) = new File(path)
  .listFiles.filter( isModuleDir).map(_.getName)

lazy val subProjects = subDirectories(".").map(name =>ProjectRef(file(name), name))

lazy val root = Project("exercism", file(".")).aggregate(subProjects : _ *)

//// Common dependencies
//libraryDependencies in ThisBuild ++= Seq(
//  "ch.qos.logback" %  "logback-classic" % "1.1.7",
//  "com.typesafe.scala-logging" % "scala-logging_2.12" % "3.5.0"
//)







