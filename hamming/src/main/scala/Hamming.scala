import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec

object Hamming extends LazyLogging {

  val bottom = '_'

  def computeOldIterative(first : String, second : String): Option[Int] = {
    val zipped = first.zipAll(second, bottom, bottom)
    logger.debug(s"Zipped: ${zipped.mkString(", ")}")
    if (zipped.exists(t => t._1 == bottom || t._2 == bottom)) None
    else (Option(zipped.count{ case (x, y) => x != y }))
  }

  def compute(first : String, second : String): Option[Int] = {

    @tailrec
    def computeTailRec(first : List[Char], second : List[Char], count: Int): Option[Int] = (first, second) match {
      case (Nil, Nil) => Option(count)
      case (f :: fs, s :: ss) if f != s => computeTailRec(fs, ss, count + 1)
      case (f :: fs, s :: ss) => computeTailRec(fs, ss, count)
      case _ => None
    }

    computeTailRec(first.toList, second.toList, 0)
  }

}
