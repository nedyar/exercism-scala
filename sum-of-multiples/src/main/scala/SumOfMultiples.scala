object SumOfMultiples {
  def sumOfMultiples(factors: Set[Int], limit: Int): Int = {
    factors
      .map(el => Stream.from(el, el))
      .map(_.takeWhile(_ < limit))
      .map(_.toList)
      .flatten
      .sum
  }
}

